package com.proj1.view;

import java.awt.event.ActionListener;

public interface ViewI {
    public void substracteListener(ActionListener actionListener);
    public void addListener(ActionListener actionListener);
    public void multiplyListener(ActionListener actionListener);
    public void divideListener(ActionListener actionListener);
    public void equalListener(ActionListener actionListener);
    public void derivateListener(ActionListener actionListener);
    public void integrateListener(ActionListener actionListener);
    public String getText();
    public void setText(String string);
    void displayErrorMessage(String errorMessage);

}
